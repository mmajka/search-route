MERGE (st_wro001:Station {stationCode: 'WRO001'})
MERGE (st_les001:Station {stationCode: 'LES001'})
MERGE (st_poz001:Station {stationCode: 'POZ001'})
MERGE (st_byd001:Station {stationCode: 'BYD001'})
MERGE (st_kon001:Station {stationCode: 'KON001'})
MERGE (st_lod001:Station {stationCode: 'LOD001'})
MERGE (st_wro001)-[:DISTANCE {distance: 100}]->(st_les001)
MERGE (st_les001)-[:DISTANCE {distance: 80}]->(st_poz001)
MERGE (st_poz001)-[:DISTANCE {distance: 200}]->(st_byd001)
MERGE (st_poz001)-[:DISTANCE {distance: 150}]->(st_kon001)
MERGE (st_kon001)-[:DISTANCE {distance: 97}]->(st_lod001)
MERGE (st_lod001)-[:DISTANCE {distance: 330}]->(st_wro001)

// WROCLAW -> BYDGOSZCZ 10-13
MERGE (st_wro001)-[:CONNECTION {connectionId: 1, departureTime: localtime('10:00:00'), arrivalTime: localtime('11:00:00')}]->(st_les001)
MERGE (st_les001)-[:CONNECTION {connectionId: 1, departureTime: localtime('11:05:00'), arrivalTime: localtime('12:00:00')}]->(st_poz001)
MERGE (st_poz001)-[:CONNECTION {connectionId: 1, departureTime: localtime('12:05:00'), arrivalTime: localtime('13:00:00')}]->(st_byd001)
// WROCLAW -> BYDGOSZCZ 12-15
MERGE (st_wro001)-[:CONNECTION {connectionId: 2, departureTime: localtime('12:00:00'), arrivalTime: localtime('13:00:00')}]->(st_les001)
MERGE (st_les001)-[:CONNECTION {connectionId: 2, departureTime: localtime('13:05:00'), arrivalTime: localtime('14:00:00')}]->(st_poz001)
MERGE (st_poz001)-[:CONNECTION {connectionId: 2, departureTime: localtime('14:05:00'), arrivalTime: localtime('15:00:00')}]->(st_byd001)
// WROCLAW -> BYDGOSZCZ 20-23
MERGE (st_wro001)-[:CONNECTION {connectionId: 3, departureTime: localtime('20:00:00'), arrivalTime: localtime('21:00:00')}]->(st_les001)
MERGE (st_les001)-[:CONNECTION {connectionId: 3, departureTime: localtime('21:05:00'), arrivalTime: localtime('22:00:00')}]->(st_poz001)
MERGE (st_poz001)-[:CONNECTION {connectionId: 3, departureTime: localtime('22:05:00'), arrivalTime: localtime('23:00:00')}]->(st_byd001)

// POZNAN -> LODZ 6-10
MERGE (st_poz001)-[:CONNECTION {connectionId: 4, departureTime: localtime('06:00:00'), arrivalTime: localtime('07:30:00')}]->(st_kon001)
MERGE (st_kon001)-[:CONNECTION {connectionId: 4, departureTime: localtime('8:00:00'), arrivalTime: localtime('10:00:00')}]->(st_lod001)
// POZNAN -> LODZ 11-15
MERGE (st_poz001)-[:CONNECTION {connectionId: 5, departureTime: localtime('11:00:00'), arrivalTime: localtime('12:30:00')}]->(st_kon001)
MERGE (st_kon001)-[:CONNECTION {connectionId: 5, departureTime: localtime('13:00:00'), arrivalTime: localtime('15:00:00')}]->(st_lod001)
// POZNAN -> LODZ 17-22
MERGE (st_poz001)-[:CONNECTION {connectionId: 6, departureTime: localtime('17:00:00'), arrivalTime: localtime('19:30:00')}]->(st_kon001)
MERGE (st_kon001)-[:CONNECTION {connectionId: 6, departureTime: localtime('20:00:00'), arrivalTime: localtime('22:00:00')}]->(st_lod001)

// LODZ -> WROCLAW -> LESZNO 5-10
MERGE (st_lod001)-[:CONNECTION {connectionId: 7, departureTime: localtime('05:00:00'), arrivalTime: localtime('08:30:00')}]->(st_wro001)
MERGE (st_wro001)-[:CONNECTION {connectionId: 7, departureTime: localtime('09:00:00'), arrivalTime: localtime('10:00:00')}]->(st_les001)
// LODZ -> WROCLAW -> LESZNO 7-12
MERGE (st_lod001)-[:CONNECTION {connectionId: 8, departureTime: localtime('07:00:00'), arrivalTime: localtime('10:30:00')}]->(st_wro001)
MERGE (st_wro001)-[:CONNECTION {connectionId: 8, departureTime: localtime('11:00:00'), arrivalTime: localtime('12:00:00')}]->(st_les001)
// LODZ -> WROCLAW -> LESZNO 20-23:30
MERGE (st_lod001)-[:CONNECTION {connectionId: 9, departureTime: localtime('20:00:00'), arrivalTime: localtime('22:30:00')}]->(st_wro001)
MERGE (st_wro001)-[:CONNECTION {connectionId: 9, departureTime: localtime('23:00:00'), arrivalTime: localtime('23:30:00')}]->(st_les001)

// LODZ -> KONIN -> LESZNO 7-13
MERGE (st_lod001)-[:CONNECTION {connectionId: 10, departureTime: localtime('07:00:00'), arrivalTime: localtime('08:30:00')}]->(st_kon001)
MERGE (st_kon001)-[:CONNECTION {connectionId: 10, departureTime: localtime('09:00:00'), arrivalTime: localtime('11:30:00')}]->(st_poz001)
MERGE (st_poz001)-[:CONNECTION {connectionId: 10, departureTime: localtime('12:00:00'), arrivalTime: localtime('13:00:00')}]->(st_les001)
// LODZ -> KONIN -> LESZNO 9-15
MERGE (st_lod001)-[:CONNECTION {connectionId: 11, departureTime: localtime('09:00:00'), arrivalTime: localtime('10:30:00')}]->(st_kon001)
MERGE (st_kon001)-[:CONNECTION {connectionId: 11, departureTime: localtime('11:00:00'), arrivalTime: localtime('13:30:00')}]->(st_poz001)
MERGE (st_poz001)-[:CONNECTION {connectionId: 11, departureTime: localtime('13:00:00'), arrivalTime: localtime('15:00:00')}]->(st_les001)
// LODZ -> KONIN -> LESZNO 16-22
MERGE (st_lod001)-[:CONNECTION {connectionId: 12, departureTime: localtime('16:00:00'), arrivalTime: localtime('17:30:00')}]->(st_kon001)
MERGE (st_kon001)-[:CONNECTION {connectionId: 12, departureTime: localtime('18:00:00'), arrivalTime: localtime('19:30:00')}]->(st_poz001)
MERGE (st_poz001)-[:CONNECTION {connectionId: 12, departureTime: localtime('20:00:00'), arrivalTime: localtime('22:00:00')}]->(st_les001)





// BYDGOSZCZ -> WROCLAW  10-13
MERGE (st_wro001)<-[:CONNECTION {connectionId: 13, departureTime: localtime('10:00:00'), arrivalTime: localtime('11:00:00')}]-(st_les001)
MERGE (st_les001)<-[:CONNECTION {connectionId: 13, departureTime: localtime('11:05:00'), arrivalTime: localtime('12:00:00')}]-(st_poz001)
MERGE (st_poz001)<-[:CONNECTION {connectionId: 13, departureTime: localtime('12:05:00'), arrivalTime: localtime('13:00:00')}]-(st_byd001)
// BYDGOSZCZ -> WROCLAW 12-15
MERGE (st_wro001)<-[:CONNECTION {connectionId: 14, departureTime: localtime('12:00:00'), arrivalTime: localtime('13:00:00')}]-(st_les001)
MERGE (st_les001)<-[:CONNECTION {connectionId: 14, departureTime: localtime('13:05:00'), arrivalTime: localtime('14:00:00')}]-(st_poz001)
MERGE (st_poz001)<-[:CONNECTION {connectionId: 14, departureTime: localtime('14:05:00'), arrivalTime: localtime('15:00:00')}]-(st_byd001)
// BYDGOSZCZ -> WROCLAW 20-23
MERGE (st_wro001)<-[:CONNECTION {connectionId: 15, departureTime: localtime('20:00:00'), arrivalTime: localtime('21:00:00')}]-(st_les001)
MERGE (st_les001)<-[:CONNECTION {connectionId: 15, departureTime: localtime('21:05:00'), arrivalTime: localtime('22:00:00')}]-(st_poz001)
MERGE (st_poz001)<-[:CONNECTION {connectionId: 15, departureTime: localtime('22:05:00'), arrivalTime: localtime('23:00:00')}]-(st_byd001)

// LODZ -> POZNAN  6-10
MERGE (st_poz001)<-[:CONNECTION {connectionId: 16, departureTime: localtime('06:00:00'), arrivalTime: localtime('07:30:00')}]-(st_kon001)
MERGE (st_kon001)<-[:CONNECTION {connectionId: 16, departureTime: localtime('8:00:00'), arrivalTime: localtime('10:00:00')}]-(st_lod001)
// LODZ -> POZNAN 11-15
MERGE (st_poz001)<-[:CONNECTION {connectionId: 17, departureTime: localtime('11:00:00'), arrivalTime: localtime('12:30:00')}]-(st_kon001)
MERGE (st_kon001)<-[:CONNECTION {connectionId: 17, departureTime: localtime('13:00:00'), arrivalTime: localtime('15:00:00')}]-(st_lod001)
// LODZ -> POZNAN 17-22
MERGE (st_poz001)<-[:CONNECTION {connectionId: 18, departureTime: localtime('17:00:00'), arrivalTime: localtime('19:30:00')}]-(st_kon001)
MERGE (st_kon001)<-[:CONNECTION {connectionId: 18, departureTime: localtime('20:00:00'), arrivalTime: localtime('22:00:00')}]-(st_lod001)

// LESZNO -> WROCLAW -> LODZ 5-10
MERGE (st_lod001)<-[:CONNECTION {connectionId: 19, departureTime: localtime('05:00:00'), arrivalTime: localtime('08:30:00')}]-(st_wro001)
MERGE (st_wro001)<-[:CONNECTION {connectionId: 19, departureTime: localtime('09:00:00'), arrivalTime: localtime('10:00:00')}]-(st_les001)
// LESZNO -> WROCLAW -> LODZ 7-12
MERGE (st_lod001)<-[:CONNECTION {connectionId: 20, departureTime: localtime('07:00:00'), arrivalTime: localtime('10:30:00')}]-(st_wro001)
MERGE (st_wro001)<-[:CONNECTION {connectionId: 20, departureTime: localtime('11:00:00'), arrivalTime: localtime('12:00:00')}]-(st_les001)
// LESZNO -> WROCLAW -> LODZ 20-23:30
MERGE (st_lod001)<-[:CONNECTION {connectionId: 21, departureTime: localtime('20:00:00'), arrivalTime: localtime('22:30:00')}]-(st_wro001)
MERGE (st_wro001)<-[:CONNECTION {connectionId: 21, departureTime: localtime('23:00:00'), arrivalTime: localtime('23:30:00')}]-(st_les001)

// LESZNO -> KONIN -> LODZ 7-13
MERGE (st_lod001)<-[:CONNECTION {connectionId: 22, departureTime: localtime('07:00:00'), arrivalTime: localtime('08:30:00')}]-(st_kon001)
MERGE (st_kon001)<-[:CONNECTION {connectionId: 22, departureTime: localtime('09:00:00'), arrivalTime: localtime('11:30:00')}]-(st_poz001)
MERGE (st_poz001)<-[:CONNECTION {connectionId: 22, departureTime: localtime('12:00:00'), arrivalTime: localtime('13:00:00')}]-(st_les001)
// LESZNO -> KONIN -> LODZ 9-15
MERGE (st_lod001)<-[:CONNECTION {connectionId: 23, departureTime: localtime('09:00:00'), arrivalTime: localtime('10:30:00')}]-(st_kon001)
MERGE (st_kon001)<-[:CONNECTION {connectionId: 23, departureTime: localtime('11:00:00'), arrivalTime: localtime('13:30:00')}]-(st_poz001)
MERGE (st_poz001)<-[:CONNECTION {connectionId: 23, departureTime: localtime('13:00:00'), arrivalTime: localtime('15:00:00')}]-(st_les001)
// LESZNO -> KONIN -> LODZ 16-22
MERGE (st_lod001)<-[:CONNECTION {connectionId: 24, departureTime: localtime('16:00:00'), arrivalTime: localtime('17:30:00')}]-(st_kon001)
MERGE (st_kon001)<-[:CONNECTION {connectionId: 24, departureTime: localtime('18:00:00'), arrivalTime: localtime('19:30:00')}]-(st_poz001)
MERGE (st_poz001)<-[:CONNECTION {connectionId: 24, departureTime: localtime('20:00:00'), arrivalTime: localtime('22:00:00')}]-(st_les001)