package pl.mmajka.innokrea.configuration;

import apoc.algo.PathFinding;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.internal.kernel.api.exceptions.KernelException;
import org.neo4j.kernel.impl.proc.Procedures;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.ogm.drivers.embedded.driver.EmbeddedDriver;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.io.File;

@Configuration
@EnableNeo4jRepositories(
        basePackages = "pl.mmajka.innokrea.repository"
)
public class Neo4jDbConfiguration {

    @Bean
    public org.neo4j.ogm.config.Configuration configuration() {
        return new org.neo4j.ogm.config.Configuration.Builder().build();
    }

    @Bean
    public SessionFactory sessionFactory(GraphDatabaseService graphDatabaseService) throws KernelException {
        registerProcedure(graphDatabaseService, PathFinding.class);
        EmbeddedDriver driver = new EmbeddedDriver(graphDatabaseService, configuration());
        return new SessionFactory(driver, "pl.mmajka.innokrea.entity");
    }


    @Bean
    public GraphDatabaseService graphDatabaseService() {
        return new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(new File("graph.db")).newGraphDatabase();
    }

    private void registerProcedure(GraphDatabaseService db, Class<?> procedure) throws KernelException {
        Procedures proceduresService = ((GraphDatabaseAPI) db).getDependencyResolver().resolveDependency(Procedures.class);
        proceduresService.registerProcedure(procedure, true);
        proceduresService.registerFunction(procedure, true);
        proceduresService.registerAggregationFunction(procedure, true);
    }

}