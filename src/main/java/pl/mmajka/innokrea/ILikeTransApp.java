package pl.mmajka.innokrea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ILikeTransApp {

    public static void main(String[] args) {
        SpringApplication.run(ILikeTransApp.class, args);
    }

}