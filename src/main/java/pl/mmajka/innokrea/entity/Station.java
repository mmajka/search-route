package pl.mmajka.innokrea.entity;

import lombok.Data;
import lombok.Getter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
public class Station {

    @Getter
    @Id
    @GeneratedValue
    private Long id;
    @Getter
    private String stationCode;
    @Getter
    @Relationship(type = "DISTANCE")
    private List<Distance> distances;
    @Getter
    @Relationship(type = "CONNECTION")
    private List<Connection> connections;

    @Override
    public String toString() {
        return "Station{" +
                "id=" + id +
                ", stationCode='" + stationCode + '\'' +
                '}';
    }
}