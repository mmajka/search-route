package pl.mmajka.innokrea.entity;

import lombok.Getter;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.List;

@QueryResult
public class DijkstraResult {

    @Getter
    private List<Station> stations;
    @Getter
    private Double weight;

}