package pl.mmajka.innokrea.entity;

import lombok.Getter;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "DISTANCE")
public class Distance {

    @Getter
    @Id
    @GeneratedValue
    private Long id;
    @Getter
    private Long distance;
    @Getter
    @StartNode
    private Station startStation;
    @Getter
    @EndNode
    private Station endStation;

    @Override
    public String toString() {
        return "Distance{" +
                "id=" + id +
                ", distance=" + distance +
                '}';
    }
}
