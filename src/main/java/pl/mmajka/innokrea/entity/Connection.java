package pl.mmajka.innokrea.entity;

import lombok.Data;
import lombok.Getter;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import java.time.LocalTime;

@RelationshipEntity(type = "CONNECTION")
public class Connection {

    @Getter
    @Id
    @GeneratedValue
    private Long id;
    @Getter
    private Long connectionId;
    @Getter
    private LocalTime departureTime;
    @Getter
    private LocalTime arrivalTime;
    @Getter
    @StartNode
    private Station startStation;
    @Getter
    @EndNode
    private Station endStation;

    @Override
    public String toString() {
        return "Connection{" +
                "id=" + id +
                ", connectionId=" + connectionId +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                '}';
    }
}