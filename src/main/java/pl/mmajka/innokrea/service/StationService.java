package pl.mmajka.innokrea.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mmajka.innokrea.entity.DijkstraResult;
import pl.mmajka.innokrea.entity.Station;
import pl.mmajka.innokrea.repository.StationRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StationService {

    private final StationRepository stationRepository;

    public DijkstraResult findShortestPath(String fromStationCode, String toStationCode) {
        return stationRepository.findShortestPathWithDijkstraAlgorithm(fromStationCode, toStationCode);
    }

    public Iterable<Station> findAllById(List<Station> stations) {
        List<Long> ids = stations.stream()
                .map(Station::getId)
                .collect(Collectors.toList());
        return stationRepository.findAllById(ids);
    }

}