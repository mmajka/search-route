package pl.mmajka.innokrea.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mmajka.innokrea.entity.Connection;
import pl.mmajka.innokrea.entity.DijkstraResult;
import pl.mmajka.innokrea.entity.Station;
import pl.mmajka.innokrea.model.RouteDetail;
import pl.mmajka.innokrea.model.exception.ConnectionNotFoundException;
import pl.mmajka.innokrea.model.exception.RouteNotFoundException;
import pl.mmajka.innokrea.service.factory.RouteDetailsFactory;

import java.time.LocalTime;
import java.util.Deque;

@Component
@RequiredArgsConstructor
public class RouteSearchService {

    private final StationService stationService;
    private final ConnectionsExtractionService connectionsExtractionService;
    private final RouteDetailsFactory routeDetailsFactory;

    public RouteDetail findShortestRoute(String fromStationCode, String toStationCode, LocalTime from) throws RouteNotFoundException, ConnectionNotFoundException {
        if (from == null) {
            from = LocalTime.now();
        }
        DijkstraResult shortestPath = stationService.findShortestPath(fromStationCode, toStationCode);
        if (shortestPath == null) {
            throw new RouteNotFoundException(String.format("Route for stations: from [%s] and to [%s] was not found.", fromStationCode, toStationCode));
        }
        Iterable<Station> stationsWithRelationships = stationService.findAllById(shortestPath.getStations());
        Deque<Connection> availableConnections = connectionsExtractionService.findAvailableConnections(stationsWithRelationships, from);
        return routeDetailsFactory.createRouteDetail(availableConnections, shortestPath.getWeight());
    }
}
