package pl.mmajka.innokrea.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Slf4j
@Component
@RequiredArgsConstructor
public class Neo4jDataInitializatonService {

    private final GraphDatabaseService graphDatabaseService;

    @Value("classpath:neo4j/init.cypher")
    private Resource cypherInitFile;

    @EventListener(ApplicationReadyEvent.class)
    public void initializeData() throws IOException {
        File file = cypherInitFile.getFile();
        String script = new String(Files.readAllBytes(file.toPath()));
        try (org.neo4j.graphdb.Transaction tx = graphDatabaseService.beginTx()) {
            graphDatabaseService.execute("MATCH (n) DETACH DELETE n");
            graphDatabaseService.execute(script);
            tx.success();
        } catch (Exception e) {
            log.error("An unexpected exception had occurred during data initialization for Neo4j database. " +
                    "Used initialization file: {}", cypherInitFile.getFilename(), e);
        }
    }

}