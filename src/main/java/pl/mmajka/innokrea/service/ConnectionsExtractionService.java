package pl.mmajka.innokrea.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.mmajka.innokrea.entity.Connection;
import pl.mmajka.innokrea.entity.Station;
import pl.mmajka.innokrea.model.exception.ConnectionNotFoundException;

import java.time.LocalTime;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;

@Slf4j
@Component
public class ConnectionsExtractionService {

    public Deque<Connection> findAvailableConnections(Iterable<Station> stations, LocalTime fromTime) throws ConnectionNotFoundException {
        Deque<Connection> result = new ArrayDeque<>();
        Station previousStation = null;
        LocalTime tempTime = LocalTime.from(fromTime);
        int counter = 0;
        for (Station currentStation : stations) {
            if (previousStation == null) {
                // we need first station to compare it later
                previousStation = currentStation;
                continue;
            }
            counter += 1;
            for (Connection currentConnection : previousStation.getConnections()) {
                if (currentConnection.getDepartureTime().isBefore(tempTime)) {
                    continue;
                }
                Connection previousConnection = result.peekLast();
                if (isCorrectDirection(previousStation, currentStation, currentConnection)) {
                    if (previousConnection == null) {
                        result.addLast(currentConnection);
                    } else if (isNewConnection(currentConnection, previousConnection)) {
                        if (isSameRoute(currentConnection, previousConnection)) {
                            result.addLast(currentConnection);
                            break;
                        } else if (currentConnection.getDepartureTime().isAfter(previousConnection.getArrivalTime())) {
                            result.addLast(currentConnection);
                        }
                    } else {
                        if (isSameRoute(currentConnection, previousConnection)) {
                            result.removeLast();
                            result.addLast(currentConnection);
                            break;
                        } else if (currentConnection.getDepartureTime().isBefore(previousConnection.getDepartureTime())) {
                            result.removeLast();
                            result.addLast(currentConnection);
                        }
                    }
                }
            }
            if (counter > result.size()) {
                throw new ConnectionNotFoundException(String.format("Connection for date %s was not found.", fromTime.toString()));
            } else {
                tempTime = result.peekLast().getArrivalTime();
            }
            previousStation = currentStation;
        }
        return result;
    }

    private boolean isSameRoute(Connection currentConnection, Connection previousConnection) {
        return Objects.equals(currentConnection.getConnectionId(), previousConnection.getConnectionId());
    }

    private boolean isNewConnection(Connection currentConnection, Connection previousConnection) {
        return Objects.equals(previousConnection.getEndStation().getStationCode(), currentConnection.getStartStation().getStationCode());
    }

    private boolean isCorrectDirection(Station previousStation, Station currentStation, Connection currentConnection) {
        return Objects.equals(currentConnection.getStartStation().getStationCode(), previousStation.getStationCode())
                && Objects.equals(currentConnection.getEndStation().getStationCode(), currentStation.getStationCode());
    }
}
