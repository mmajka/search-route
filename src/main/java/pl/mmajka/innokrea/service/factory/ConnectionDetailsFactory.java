package pl.mmajka.innokrea.service.factory;

import org.springframework.stereotype.Component;
import pl.mmajka.innokrea.entity.Connection;
import pl.mmajka.innokrea.model.ConnectionDetail;
import pl.mmajka.innokrea.model.ConnectionDetailsResult;
import pl.mmajka.innokrea.model.StationDetail;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Component
public class ConnectionDetailsFactory {

    public ConnectionDetailsResult createConnectionDetails(Deque<Connection> connections) {
        Map<Long, ConnectionDetail> result = new LinkedHashMap<>();
        Set<StationDetail> allStops = new LinkedHashSet<>();
        for (Connection connection : connections) {
            ConnectionDetail connectionDetail = result.get(connection.getConnectionId());
            StationDetail startStation = new StationDetail(connection.getStartStation());
            StationDetail endStation = new StationDetail(connection.getEndStation());
            if (Objects.isNull(connectionDetail)) {
                connectionDetail = ConnectionDetail.builder()
                        .connectionId(connection.getConnectionId())
                        .arrivalDate(connection.getArrivalTime())
                        .departureDate(connection.getDepartureTime())
                        .startStation(startStation)
                        .build();
                result.put(connection.getConnectionId(), connectionDetail);
                connectionDetail.addStop(startStation);
                allStops.add(startStation);
            } else {
                connectionDetail.setArrivalDate(connection.getArrivalTime());
            }
            allStops.add(endStation);
            connectionDetail.setEndStation(endStation);
            connectionDetail.addStop(endStation);
        }

        List<ConnectionDetail> connectionDetails = new ArrayList<>(result.values());
        return new ConnectionDetailsResult(connectionDetails, allStops);
    }
}
