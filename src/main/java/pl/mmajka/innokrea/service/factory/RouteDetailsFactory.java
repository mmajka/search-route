package pl.mmajka.innokrea.service.factory;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mmajka.innokrea.entity.Connection;
import pl.mmajka.innokrea.model.ConnectionDetail;
import pl.mmajka.innokrea.model.ConnectionDetailsResult;
import pl.mmajka.innokrea.model.RouteDetail;

import java.util.Deque;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RouteDetailsFactory {

    private final ConnectionDetailsFactory connectionDetailsFactory;

    public RouteDetail createRouteDetail(Deque<Connection> availableConnections, Double distance) {
        ConnectionDetailsResult connectionDetailsResult = connectionDetailsFactory.createConnectionDetails(availableConnections);
        RouteDetail.RouteDetailBuilder routeDetailBuilder = RouteDetail.builder();
        List<ConnectionDetail> connectionDetails = connectionDetailsResult.getConnectionDetails();
        ConnectionDetail connectionDetail = connectionDetails.get(0);
        if (connectionDetails.size() == 1) {
            routeDetailBuilder.arrivalTime(connectionDetail.getArrivalDate())
                    .endStation(connectionDetail.getEndStation());
        } else {
            int lastIndex = connectionDetails.size() - 1;
            ConnectionDetail lastConnection = connectionDetails.get(lastIndex);
            routeDetailBuilder.arrivalTime(lastConnection.getArrivalDate())
                    .endStation(lastConnection.getEndStation());
        }
        return routeDetailBuilder.startStation(connectionDetail.getStartStation())
                .departureDate(connectionDetail.getDepartureDate())
                .connectionDetails(connectionDetails)
                .allStops(connectionDetailsResult.getAllStops())
                .completeDistance(distance.longValue())
                .build();
    }

}
