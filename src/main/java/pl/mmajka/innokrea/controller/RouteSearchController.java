package pl.mmajka.innokrea.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.mmajka.innokrea.model.RouteDetail;
import pl.mmajka.innokrea.model.exception.ConnectionNotFoundException;
import pl.mmajka.innokrea.model.exception.RouteNotFoundException;
import pl.mmajka.innokrea.service.RouteSearchService;

import java.time.LocalTime;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RouteSearchController {

    private final RouteSearchService routeSearchService;

    @GetMapping(value = "/route", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RouteDetail> routeSearchCall(
            @RequestParam("from") String fromStationCode,
            @RequestParam("to") String toStationCode,
            @RequestParam(value = "date", required = false) @DateTimeFormat(pattern = "HH:mm:ss") LocalTime date
    ) {
        ResponseEntity<RouteDetail> responseEntity;
        try {
            RouteDetail result = routeSearchService.findShortestRoute(fromStationCode, toStationCode, date);
            responseEntity = new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RouteNotFoundException | ConnectionNotFoundException e) {
            RouteDetail result = RouteDetail.builder().message(e.getMessage()).build();
            responseEntity = new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error("An unexpected exception took place.", e);
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}
