package pl.mmajka.innokrea.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import pl.mmajka.innokrea.entity.DijkstraResult;
import pl.mmajka.innokrea.entity.Station;

public interface StationRepository extends Neo4jRepository<Station, Long> {

    @Query("MATCH (from:Station {stationCode: $fromStationCode}), (to:Station {stationCode: $toStationCode}) " +
            "CALL apoc.algo.dijkstra(from, to,'DISTANCE', 'distance') " +
            "YIELD path, weight " +
            "RETURN nodes(path) as stations, weight")
    DijkstraResult findShortestPathWithDijkstraAlgorithm(@Param("fromStationCode") String fromStationCode, @Param("toStationCode") String toStationCode);

}
