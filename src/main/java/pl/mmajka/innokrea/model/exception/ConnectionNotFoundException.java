package pl.mmajka.innokrea.model.exception;

public class ConnectionNotFoundException extends Exception {

    public ConnectionNotFoundException(String message) {
        super(message);
    }
}
