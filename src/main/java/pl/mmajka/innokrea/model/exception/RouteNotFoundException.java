package pl.mmajka.innokrea.model.exception;

public class RouteNotFoundException extends Exception {

    public RouteNotFoundException(String message) {
        super(message);
    }
}
