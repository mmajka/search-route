package pl.mmajka.innokrea.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Builder
@Data
public class ConnectionDetail {

    private Long connectionId;
    private LocalTime departureDate;
    private LocalTime arrivalDate;
    private StationDetail startStation;
    private StationDetail endStation;
    private List<StationDetail> stops = new LinkedList<>();

    public void addStop(StationDetail station){
        if (stops == null){
            stops = new LinkedList<>();
        }
        stops.add(station);
    }

}
