package pl.mmajka.innokrea.model;

import lombok.Data;
import pl.mmajka.innokrea.entity.Station;

@Data
public class StationDetail {

    private String stationCode;

    public StationDetail(Station station) {
        this.stationCode = station.getStationCode();
    }
}