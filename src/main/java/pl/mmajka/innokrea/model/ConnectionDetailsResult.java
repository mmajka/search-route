package pl.mmajka.innokrea.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
public class ConnectionDetailsResult {

    @Getter
    private List<ConnectionDetail> connectionDetails;
    @Getter
    private Set<StationDetail> allStops;

}
