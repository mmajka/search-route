package pl.mmajka.innokrea.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;
import java.util.List;
import java.util.Set;

@Builder
@Data
public class RouteDetail {

    private String message;
    private List<ConnectionDetail> connectionDetails;
    private Set<StationDetail> allStops;
    private StationDetail startStation;
    private StationDetail endStation;
    private LocalTime departureDate;
    private LocalTime arrivalTime;
    private Long completeDistance;

}
